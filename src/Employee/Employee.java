/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Employee;


public class Employee {
   private String name;
   private double hourlyWage;
   private int hoursWorked;
   
   Employee(){
       
   }
   
   Employee(String name, double hourlyWage, int hoursWorked){
       this.name = name;
       this.hourlyWage = hourlyWage;
       this.hoursWorked = hoursWorked;
   }
   
   public double calculatePay(){
      return hoursWorked * hourlyWage;
   }
   
}


