/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Employee;


public class PayrollSimulation {
    
    public static void main(String[] args){
        Employee emp1 = new Employee("Harry", 15.0, 40);
        Manager manager1 = new Manager("Carl", 20.0, 40, 5.0);
        
        System.out.println(emp1.calculatePay());
         System.out.println( manager1.calculatePay());
        
    }
}


